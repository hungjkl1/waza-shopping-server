require('dotenv').config()
var express = require('express');
var app = express();
var ParseServer = require('parse-server').ParseServer;
var ParseDashboard = require('parse-dashboard')
var ParseServerConfig = require('./parse-config/parse-server')
var ParseDashboardConfig = require('./parse-config/parse-dashboard')
/**
 * server 
 */
var parseAPI = new ParseServer(ParseServerConfig)
app.use('/api', parseAPI)

var parseDashboard = new ParseDashboard(ParseDashboardConfig,{allowInsecureHTTP: true })
app.use('/dashboard', parseDashboard)

const port = process.env.PORT || 3000

var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('parse-server-example running on port ' + port + '.');
});

ParseServer.createLiveQueryServer(httpServer);