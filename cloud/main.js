const _ = require('lodash')

/*Parse*/
Parse.Cloud.define("hello", function (request) {
  return "world!";
});

//User API
const User = require('./modules/User')
Parse.Cloud.define('createAdmin', User.createAdmin)
Parse.Cloud.define('createMod', User.createModerator)
Parse.Cloud.define('getAdmins', User.getAdmins)
Parse.Cloud.define('countAdmin', User.countAdmin)
Parse.Cloud.define('loginAdmin',User.adminLogin)
Parse.Cloud.define('getAdmin',User.getAdmin)
Parse.Cloud.define('editAdmin',User.changeAdminDetail)
Parse.Cloud.define('deleteAdmin',User.deleteAdmin)
Parse.Cloud.define('userSignin',User.userSignin)
Parse.Cloud.define('userLogin',User.passengerLogin)
Parse.Cloud.define('addWallet',User.addWazaWallet)
Parse.Cloud.define('searchByEmail',User.searchByEmail)
Parse.Cloud.define('getPassengers',User.getPassengers)
Parse.Cloud.define('getPassenger',User.getPassenger)
Parse.Cloud.define('getPassengersByArray',User.getPassengersByArray)



//Shop API
const Shop = require('./modules/Shop');
Parse.Cloud.define('createShop', Shop.createShop);
Parse.Cloud.define('getActiveShops', Shop.getActiveShop);
Parse.Cloud.define('getShops',Shop.getShop);
Parse.Cloud.define('getShop',Shop.getShopById);
Parse.Cloud.define('removeShop',Shop.removeShop);
Parse.Cloud.define('updateShop',Shop.updateShop);
Parse.Cloud.define('getShopFromArray',Shop.getShopFromArray)
Parse.Cloud.define('searchShop',Shop.SearchShop)
Parse.Cloud.define('getShopDetail',Shop.getShopDetail)



//ProductAPI
const Products = require('./modules/Products')
Parse.Cloud.define('createProduct', Products.createNewproduct)
Parse.Cloud.define('getProducts',Products.getProducts)
Parse.Cloud.define('getProduct',Products.getProduct)
Parse.Cloud.define('updateProduct',Products.updateProduct)
Parse.Cloud.define('deleteProduct',Products.removeProduct)



//BillAPI
const Bill = require('./modules/Bill');
Parse.Cloud.define('createBill',Bill.createNewBill)
Parse.Cloud.define('getBills',Bill.getBills)
Parse.Cloud.define('getUserBills',Bill.getUserBills)
Parse.Cloud.define('updateBill',Bill.billUpdate)
Parse.Cloud.define('getBill',Bill.getBillDetail)
Parse.Cloud.define('getBillAdmin',Bill.getBill)
Parse.Cloud.define('updateBillState', Bill.updateBillState)
Parse.Cloud.define('updateBillDriver', Bill.setBill)




//LocationAPI

const LocationUser = require('./modules/LocationUser')
Parse.Cloud.define('createLocation',LocationUser.CreateLocation)
Parse.Cloud.define('deleteLocation',LocationUser.DeleteLocation)
Parse.Cloud.define('getLocation',LocationUser.getLocation)

//listDriverApi
const driverModule = require('./modules/Driver')
Parse.Cloud.define('listDriverForWait',driverModule.getListOfDriverAcceptTrip)
Parse.Cloud.define('acceptTrip',driverModule.acceptTrip)
Parse.Cloud.define('rejectTrip',driverModule.rejectTrip)


Parse.Cloud.job('Add lower case', async (request, status) => {
  try {
    const User = new Parse.User()
    const query = new Parse.Query(User)
    const users = await query.find({useMasterKey:true})
    users.forEach(element => {
      const email = element.get('email')
      element.set('lowerCase_email', String(email).toLowerCase())
    });
    saveUsers = await Parse.Object.saveAll(users,{useMasterKey:true})
    return 'Done'
  } catch (error) {
    console.log(error)
  }
})

Parse.Cloud.job('Update wallet', async (request, status) => {
  try {
    let skip = 0
    let isQuery = true
    while (isQuery) {
      const passengerQuery = new Parse.Query(new Parse.User())
      passengerQuery.equalTo('userType', 'PASSENGER')
      passengerQuery.limit(10)
      passengerQuery.skip(skip*10)
      const passengers = await passengerQuery.find()
      if(_.isEmpty(passengers)) {
        isQuery=false
      } else {
        passengers.map(async item => {
          try {
            console.log(process.env.PAYMENT_URL)
            console.log(item.get('coreId'))
            const createWallet = await Parse.Cloud.httpRequest({
              url: `${process.env.PAYMENT_URL}create_wallet`,
              headers: {
                "Content-Type": "application/json;charset=utf-8"
              },
              body: {
                'user_id': item.get('coreId')
              },
              method: "post"
            })
            console.log(createWallet.data)
          }
          catch (err) {
            console.log(err.text)
          }
        })
        isQuery=false
      }
    }
  } catch (error) {
    console.log(error)
  }
})