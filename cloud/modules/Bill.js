const billModule = {}
const _ = require('lodash')

billModule.createNewBill = async request => {
  const {user} = request
  if(user){
    //Bill and bill detail parse object
    const Bill = Parse.Object.extend("Bill")
    const BillDetail = Parse.Object.extend("BillDetail")
    //Product Shop parse object
    const Product = Parse.Object.extend('Product')
    const Shop = Parse.Object.extend('Shop')


    const ShopOld = new Shop()
    const newBill = new Bill()
    
    const {billDetail=[]} = request.params
    
    //create new bill
    for(let key in request.params){
      if(key!=='billDetail'&&key!=='location') {
        newBill.set(key,request.params[key])
      }
    }
    newBill.set('user',user)
    //add bill detail
    let billDetails = []
    if(!_.isEmpty(billDetail)) {
      let shopId = null
      billDetail.forEach(element => {
        const newBillDetail = new BillDetail()
        const productOld = new Product()
        productOld.id = element.objectId
        newBillDetail.set('product',productOld)
        newBillDetail.set('price',element.price)
        newBillDetail.set('productName',element.name)
        newBillDetail.set('bill',newBill)
        newBillDetail.set("quantity",element.quantity)
        billDetails.push(newBillDetail)
        if(!shopId){
          shopId = element.shop.objectId
        }
      });
      ShopOld.id = shopId
      newBill.set("shop",ShopOld)
    }
    console.log(billDetails)
    //save shop
    return Parse.Object.saveAll(billDetails,{useMasterkey:true}).then((result) => {
      return result
    }).catch((err) => {
      console.log(err)
      return err
    });
  } else {
    return 'Please login to use this feature'
  }
}

billModule.getBills = async request => {
  const {pagination={}, filter={}} = request.params
  const Bill = Parse.Object.extend("Bill")
  const query = new Parse.Query(Bill)
  query.limit(pagination.perPage ? pagination.perPage : 100)
  query.skip(pagination.page ? (pagination.page-1)*pagination.perPage : 0)
  query.descending("createdAt")
  query.withCount()
  try{
    const result = await query.find()
    return result
  } catch(err){
    return err
  }
}

billModule.getUserBills = async request => {
  const {user={}} = request
  const {pagination={}, filter={}} = request.params
  const Bill = Parse.Object.extend("Bill")
  const query = new Parse.Query(Bill)
  if(user){
      query.equalTo('user',user)
  }
  query.limit(pagination.perPage ? pagination.perPage : 100)
  query.skip(pagination.page ? (pagination.page-1)*pagination.perPage : 0)
  query.descending("createdAt")
  query.include("shop")
  query.withCount()
  try{
    const result = await query.find()
    return result
  } catch(err){
    return err
  }
}

billModule.billUpdate = async request => {
  const {id,driverId,state} = request.params
  const Bill = Parse.Object.extend("Bill")
  const queryBill = new Parse.Query(Bill)
  queryBill.equalTo('objectId',id)
  try{
    const billToUpdate = await queryBill.first()
    if(driverId){
      user = new Parse.User()
      user.id = driverId
      billToUpdate.set("driver",user)
    }
    billToUpdate.set('state',state)
    return billToUpdate.save(null,{useMasterkey:true})
  } catch(err) {
    return err
  }
}



billModule.getBillDetail = async request => {
  const {id} = request.params
  try{
    const Bill =  Parse.Object.extend("Bill")
    const BillToFind = new Bill()
    BillToFind.id = id
    const BillDetail = Parse.Object.extend("BillDetail")
    const query = new Parse.Query(BillDetail)
    query.equalTo("bill",BillToFind)
    query.withCount()
    query.include('product')
    const yourBill = await query.find();

    return yourBill
  } catch(error) {
    return error
  }
}

billModule.updateBillState = async request => {
  const {id, State} = request.params
  console.log(id,State)
  try{
    const Bill =  Parse.Object.extend("Bill")
    const BillToFind = new Bill()
    BillToFind.id = id
    const query = new Parse.Query(BillToFind)
    query.equalTo("objectId",id)
    
    const yourBill = await query.first();
    yourBill.set('State', State)
    const update = await yourBill.save(null,{useMasterkey:true})
    return update
  } catch(error) {
    return error
  }
}

billModule.getBill = async request => {
  const {id} = request.params
  try {
    const Bill =  Parse.Object.extend("Bill")
    const query =  new Parse.Query(Bill)
    query.equalTo('objectId', id)
    query.include('shop')
    query.include('user')
    const bill = await query.first()
    return bill
  } catch (e) {
    console.log(e)
    return "Cant find any bill related"
  }
}

billModule.setBill = async request => {
  const {id, driverId, driver} = request.params
  try {
    const Bill =  Parse.Object.extend("Bill")
    const query =  new Parse.Query(Bill)
    query.equalTo('objectId', id)
    const bill = await query.first()
    bill.set('driverId',driverId)
    bill.set('driver',driver)
    bill.set('State','ORDER HAVE DRIVER')
    const save =  await bill.save(null,{useMasterkey:true})
    return save
  } catch (error) {
    console.log(error)
    return error
  }
}

module.exports = billModule