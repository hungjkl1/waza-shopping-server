"use strict";
var jwtDecode = require("jwt-decode");

var _ = require("lodash");


const provider = id => ({
  authenticate: () => Promise.resolve(),
  restoreAuthentication() {
    return true;
  },

  getAuthType() {
    return "core";
  },

  getAuthData() {
    return {
      authData: {
        id: id
      }
    };
  }
});

const UserModule = {};
//Admin module
UserModule.createAdmin = request => {
  const { username, password, email, idCard, phone } = request.params;
  const newAdmin = new Parse.User();
  newAdmin.set("username", username);
  newAdmin.set("password", password);
  newAdmin.set("email", email);
  newAdmin.set("idCard", idCard);
  newAdmin.set("phone", phone);

  newAdmin.set("userType", "ADMIN");

  return newAdmin
    .signUp()
    .then(result => {
      return result;
    })
    .catch(err => {
      return err;
    });
};

UserModule.createModerator = request => {
  const { username, password, email, idCard, phone } = request.params;
  const newModerator = new Parse.User();
  newModerator.set("username", username);
  newModerator.set("password", password);
  newModerator.set("email", email);
  newModerator.set("userType", "MODERATOR");
  newAdmin.set("idCard", idCard);
  newAdmin.set("phone", phone);

  return newModerator
    .signUp()
    .then(result => {
      return "Create Moderator Successfully";
    })
    .catch(err => {
      return err;
    });
};

UserModule.getAdmins = async request => {
  const { pagination, sort, filter } = request.params;
  const admin = new Parse.User();
  const query = new Parse.Query(admin);
  query.equalTo("userType", "ADMIN");
  query.limit(pagination.perPage);
  query.skip(pagination.page ? (pagination.page - 1) * pagination.perPage : 0);
  query.startsWith(
    "username",
    _.isUndefined(filter.username) ? "" : String(filter.username)
  );
  query.withCount();
  try {
    const admins = await query.find();
    return admins;
  } catch (err) {
    console.log(err);
  }
};
UserModule.adminLogin = async request => {
  const { username, password } = request.params;
  try {
    let admin = await Parse.User.logIn(username, password);
    admin = admin.toJSON();
    if (admin.userType === "ADMIN" || admin.userType === "MODERATOR") {
      return admin;
    } else {
      throw new Error("Only admin or moderator can access this page");
    }
  } catch (error) {
    throw new Error(error);
  }
};

UserModule.userSignin = async request => {
  const { username, password, email, phone, fullName } = request.params;
  try {
    const coreUser = await Parse.Cloud.httpRequest({
      url: `${process.env.CORE_URL}/passengers`,
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      body: request.params,
      method: "post"
    });
    const { data = null } = coreUser;
    if(coreUser.status===200){
      throw ('Account already existed in waza system please login with your waza account')
    }
    if (data) {
      const authProvider = provider(data._id);
      Parse.User._registerAuthenticationProvider(authProvider);
      const newAdmin = new Parse.User();
      newAdmin.set("fullName", fullName);
      newAdmin.set("username", username);
      newAdmin.set("password", password);
      newAdmin.set("email", email);
      newAdmin.set("phone", phone);
      newAdmin.set("coreId", data._id);
      newAdmin.set("userType", "PASSENGER");
      const saveAdmin = await newAdmin._linkWith(
        authProvider.getAuthType(),
        authProvider.getAuthData(),
        { useMasterKey: true }
      );
      console.log(coreUser.status)
      return saveAdmin;
    }
    throw ("Failed to connect to core server");
  } catch (err) {
    const { data = null } = err;
    if (data) {
      return err.data;
    }
    return err;
  }
};

UserModule.passengerLogin = async request => {
  console.log(process.env.CORE_URL)
  try {
    const coreUser = await Parse.Cloud.httpRequest({
      url: `${process.env.CORE_URL}/passengers/auth`,
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      },
      body: request.params,
      method: "post"
    });
    if (coreUser.status === 200) {
      const coreUserInfo = await Parse.Cloud.httpRequest({
        url: `${process.env.CORE_URL}/passengers?_id=${coreUser.data.id}`,
        headers: {
          "Content-Type": "application/json;charset=utf-8",
          "Authorization": coreUser.data.token
        },
        method: "get"
      });
      const queryUser = new Parse.Query(Parse.User);
      queryUser.equalTo("coreId", coreUser.data.id);
      const user = await queryUser.first();
      const userCore = jwtDecode(coreUser.data.token);
      if (user) {
        for (let key in coreUserInfo.data[0]) {
          user.set(key, coreUserInfo.data[0][key]);
        }
        const saveNewInfo = await user.save(null, { useMasterKey: true });
        const newLogin = Parse.User.logInWith("core", {
          authData: {
            id: userCore._id
          }
        });
        return newLogin;
      }
      console.log(coreUserInfo.data)
      const authProvider = provider(userCore._id);
      Parse.User._registerAuthenticationProvider(authProvider);
      const newPassenger = new Parse.User();
      for (let key in coreUserInfo.data[0]) {
        newPassenger.set(key, coreUserInfo.data[0][key]);
      }
      newPassenger.set("coreId", coreUserInfo.data[0]._id);
      newPassenger.set("userType", "PASSENGER");
      const saveNewPassenger = await newPassenger._linkWith(
        authProvider.getAuthType(),
        authProvider.getAuthData(),
        { useMasterKey: true }
      );
      return saveNewPassenger
    }
  } catch (error) {
    const { data = null } = error;
    if (data) {
      return data;
    }
    console.log(error);
    return error;
  }
};

UserModule.changeAdminDetail = async request => {
  const { objectId } = request.params;
  const query = new Parse.Query(admin);
  query.equalTo("objectId", objectId);
  const user = await query.first();
  for (let key in request.params) {
    if (request.params[key]) {
      if (key === "phone") {
        user.set(key, Number(request.params[key]));
      } else user.set(key, request.params[key]);
    }
  }
  return user.save(null, { useMasterKey: true });
};
UserModule.getAdmin = async request => {
  const { id } = request.params;
  const admin = new Parse.User();
  const query = new Parse.Query(Parse.User);
  query.equalTo("objectId", id);
  const result = await query.first({ useMasterKey: true });
  return result;
};

UserModule.deleteAdmin = async request => {
  const { id } = request.params;
  const admin = new Parse.User();
  const query = new Parse.Query(admin);
  query.equalTo("objectId", id);
  const result = await query.first();
  return result
    .destroy({ useMasterKey: true })
    .then(result => {
      return "Admin Deleted";
    })
    .catch(err => {
      return err;
    });
};

UserModule.addWazaWallet = async request => {
  try {
    const {user} = request;
    if(!user) throw ('Please login to charge your wallet')
    const {money} = request.params
    if(!money||money<50000) throw ('Please charge more than 50.000VND')
    const currentWallet = user.get('wallet')
    user.set('wallet',currentWallet+Number(money))
    const updateWallet = await user.save(null,{useMasterKey:true})
    return updateWallet
  } catch(error) {
    return error
  }
}

UserModule.searchByEmail = async request => {
  try {
    const {email} = request.params
    const query = new Parse.Query(new Parse.User())
    query.contains('email', email)
    const user = await query.find({useMasterKey:true})
    return user
  } catch (error) {
    console.log(error)
  }
}

UserModule.getPassengers = async request => {
  const { pagination = {}, sort = {}, filter = {} } = request.params;
  const passenger = new Parse.User();
  const query = new Parse.Query(passenger);
  query.equalTo("userType", "PASSENGER");
  query.limit(pagination.perPage);
  query.skip(pagination ? (pagination.page - 1) * pagination.perPage : 0);
  // query.startsWith(
  //   "fullName",
  //   filter.username ? "" : String(filter.username)
  // );
  query.withCount();
  try {
    const passengers = await query.find();
    return passengers;
  } catch (err) {
    console.log(err);
  }
}

UserModule.getPassengersByArray = async request => {
  const {ids} = request.params;
  const passenger = new Parse.User();
  const query = new Parse.Query(passenger);
  query.containedIn("objectId", ids);
  try {
    const passengers = await query.find();
    return passengers;
  } catch (err) {
    console.log(err);
  }
}

UserModule.getPassenger = async request => {
  const { id } = request.params;
  const query = new Parse.Query(Parse.User);
  query.equalTo("objectId", id);
  const result = await query.first({ useMasterKey: true });
  return result;
}

module.exports = UserModule;
