const driverModule = {}
const _ = require('lodash')

driverModule.getListOfDriverAcceptTrip = async request => {
    try {
        const {
            service_trip_id,
            list_driver,
            service,
            start_lat,
            start_lon,
            end_lat,
            end_lon,
            start_addresss,
            end_address,
            passenger_id,
            price,
            payment_type,
            distance} = request.params
        const driverAcceptList = new Parse.Object('DriverList')
        const Bill = new Parse.Object('Bill')
        Bill.set('objectId',service_trip_id.substring(9,service_trip_id.length))
        driverAcceptList.set('service_trip_id',Bill)
        driverAcceptList.set('list_driver',list_driver)
        driverAcceptList.set('service',`${service}`)
        driverAcceptList.set('start_lat',start_lat)
        driverAcceptList.set('start_lon',start_lon)
        driverAcceptList.set('end_lat',end_lat)
        driverAcceptList.set('end_lon',end_lon)
        driverAcceptList.set('start_addresss',start_addresss)
        driverAcceptList.set('end_address',end_address)
        driverAcceptList.set('passenger_id',passenger_id)
        driverAcceptList.set('price',price)
        driverAcceptList.set('payment_type',payment_type)
        driverAcceptList.set('distance',distance)
        driverAcceptList.set('currentDriver',list_driver[0])
        driverAcceptList.set('active',true)
        const save = await driverAcceptList.save(null,{useMasterkey:true})
        return 'success'
    } catch (err) {
        console.log(err)
        return err
    }
}
driverModule.acceptTrip = async request => {
 try {
    const {id} = request.params
    const driverAcceptList =new Parse.Object('DriverList')
    const query = new Parse.Query(driverAcceptList)
    query.equalTo('objectId',id)
    const driversAccept = await query.first()
    driversAccept.set('haveAcceptDriver', true)
    const save = await driversAccept.save(null,{useMasterkey:true})
    return save
 } catch (err) {
     console.log(err)
    return 'Driver Accept Failed'

 }
}

driverModule.rejectTrip = async request => {
    try {
       const {id} = request.params
       console.log(request.params)
       const driverAcceptList =new Parse.Object('DriverList')
       const query = new Parse.Query(driverAcceptList)
       query.equalTo('objectId',id)
       const driversAccept = await query.first()
       let list_rejected = driversAccept.get('list_driver_rejected')
       console.log(list_rejected);
        list_rejected.push(driversAccept.get('currentDriver'))
       driversAccept.set('list_driver_rejected', list_rejected)
       if(list_rejected.length === driversAccept.get('list_driver').length) {
        driversAccept.set('haveAcceptDriver', false)
        driversAccept.set('currentDriver',null)
       } else if(list_rejected.length < driversAccept.get('list_driver').length) {
        driversAccept.set('currentDriver',driversAccept.get('list_driver')[list_rejected.length])
       }
       const save = await driversAccept.save(null,{useMasterkey:true})
       return save
    } catch (err) {
        console.log(err)
       return 'Driver Rejected Failed'
   
    }
   }

module.exports = driverModule;