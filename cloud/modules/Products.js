'use strict'

const productModule = {}
const _ = require('lodash')

productModule.createNewproduct = async request => {
    const {shop, name, price, description, type, Unit} = request.params
    const shops = Parse.Object.extend('Shop')
    const query = new Parse.Query(shops)
    if(!name||!price){
        throw new Error('name or price are required')
    }
    if(!shop){
        throw new Error('shop needed')
    }
    query.equalTo('objectId',shop.objectId)
    try {
        const shop = await query.first()
        const product = Parse.Object.extend('Product')
        const newProduct = new product()
        newProduct.set('name',name)
        newProduct.set('price',Number(price))
        newProduct.set('description',description)
        newProduct.set('shop',shop)
        newProduct.set('Unit',Unit)
        newProduct.set('type',type)
        const saveProduct = await newProduct.save()
        return saveProduct
    } catch(err){
        return err
    }
}

productModule.getProducts = async request => {
    const {id,pagination={}, filter={}} = request.params
    const shops = Parse.Object.extend('Shop')
    const product = Parse.Object.extend('Product')
    const query = new Parse.Query(product)
    if (!_.isUndefined(id)) {
        const shop = new shops()
        shop.set('id',id)
        query.equalTo('shop',shop)
    }
    query.equalTo('deleted',false)
    query.include('shop')
    query.limit(pagination.perPage ? pagination.perPage: 100)
    query.skip(pagination.page ? (pagination.page-1)*pagination.perPage : 0)
    query.startsWith('name', _.isUndefined(filter.name) ? '' : String(filter.name))
    query.withCount()
    const products = await query.find()
    return products
}

productModule.getProduct = async request => {
    const {id} = request.params
    if (!id) {
        throw new Error('Cant find any product related')
    }
    const product = Parse.Object.extend('Product')
    const query = new Parse.Query(product)
    query.equalTo('objectId',id)
    query.withCount()
    const products = await query.first()
    return products
}

productModule.updateProduct = async request => {
    const {objectId} = request.params
    if(!objectId){
        throw new Error('Cant find any product related')
    }
    const products = Parse.Object.extend('Product')
    const query = new Parse.Query(products)
    query.equalTo('objectId',objectId)
    const product = await query.first()
    for(let key in request.params){
        if(key!=='shop') {
        if(key !== 'price') {
            console.log(key)
            product.set(key,request.params[key])
        } else if(key==='price') {
            product.set(key,Number(request.params[key]))
        }
        }
    }
    return product.save(null,{useMasterKey:true}).then((result) => {
        return 'Update Success'
    }).catch((err) => {
        return err
    });
}

productModule.removeProduct = async (request) => {
    const products = Parse.Object.extend('Product');
    const query = new Parse.Query(products)
  
    // Find shop by id
    query.equalTo('objectId', request.params.id);
  
    // Get the fist shop in query
    const product = await query.first();
    if (product) {
      // Change active = false 
      product.set('deleted', true);
      // Save product
      return product.save()
        .then((productRemoved) => {
          return productRemoved
        })
        .catch((error) => {
          return error
        })
    } else {
      throw 'Product not found';
    };
  };

module.exports = productModule