'use-strict'
 
const locationUser = {}

locationUser.CreateLocation = async request => {
    const {user} = request
    const {lng,lat,address} =  request.params
    if(user){
        if(lng&&lat&&address){
            const location = Parse.Object.extend('Location')
            const newLocation = new location()
            newLocation.set('lng',lng)
            newLocation.set('lat',lat)
            newLocation.set('address',address)
            var point = new Parse.GeoPoint({latitude: Number(lat), longitude: Number(lng)});
            newLocation.set('locatio yn',point)
            newLocation.set('user',user)
            newLocation.set('active',true)
            try {
                const result = await newLocation.save(null,{useMasterKey: true})
                return result
            } catch(err) {
                return err
            }
        } else throw new Error('Please provide all needed informations')

    } else throw new Error('Please login again to use this feature')
}

locationUser.DeleteLocation = async request => {
    const {user} = request
    const {shopId} = request.params
    console.log(shopId)
    if(user) {
        const location = Parse.Object.extend('Location')
        const query = new Parse.Query(location)
        query.equalTo('objectId',shopId)
        query.equalTo('user',user)
        try {
            const myLocation = await query.first()
            myLocation.set('active',false)
            const result = myLocation.save(null,{useMasterKey: true})
            return result
        } catch(err) {
            return err
        }
    } else throw new Error('Please login again to use this feature')

}

locationUser.getLocation = async request => {
    const {user} = request
    if(user) {
        const location = Parse.Object.extend('Location')
        const query = new Parse.Query(location)
        query.equalTo('user',user)
        query.equalTo('active',true)
        try {
            const myLocation = await query.find()
            return myLocation
        } catch(err) {
            return err
        }
    } else throw new Error('Please login again to use this feature')

}

module.exports = locationUser