'use strict'
const _ = require('lodash')

const ShopModule = {}

// CREATE SHOP 
ShopModule.createShop = (request) => {
  const {
    name, // can update
    address, // can update
    phone, // can update
    openTime, // can update 
    closeTime, // can update
    socialInfo, // can update
    imageURL, // can update
    status, // admin can update
    lat,
    lng
  } = request.params

  const Shop = Parse.Object.extend('Shop');
  const newShop = new Shop();
  console.log(lat)
  console.log(lng)

  var point = new Parse.GeoPoint({latitude: Number(lat), longitude: Number(lng)});

  newShop.set('name', name);
  newShop.set('address', address);
  newShop.set('phone', phone);
  // newShop.set('openTime', openTime);
  // newShop.set('closeTime', closeTime);
  newShop.set('socialInfo', socialInfo);
  newShop.set('imageURL', imageURL);
  newShop.set('status', status);
  newShop.set('active', true);
  newShop.set('ltn',lat)
  newShop.set('lng',lng)
  newShop.set('location',point)

  return newShop.save()
    .then((result) => {
      return 'Create shop successfully'
    })
    .catch((error) => {
      return error
    })
}

// GET SHOP LIST
ShopModule.getShop = () => {
  const shops = Parse.Object.extend('Shop');
  const query = new Parse.Query(shops)

  // get all shops
  return query.find()
    .then((resultShop) => {
      return resultShop
    })
    .catch((error) => {
      return error
    })
};

// GET ACTIVE SHOP LIST
ShopModule.getActiveShop = request => {
  const {pagination, filter={}} = request.params
  const shops = Parse.Object.extend('Shop');
  const query = new Parse.Query(shops);

  // get shops active == true
  query.equalTo('active', true);
  query.limit(pagination.perPage)
  query.skip(pagination.page ? (pagination.page-1)*pagination.perPage : 0)
  query.startsWith('name', _.isUndefined(filter.name) ? '' : String(filter.name))
  query.withCount()
  return query.find()
    .then((result) => {
      return result;
    })
    .catch((error)=>{
      return error
    })
};

// GET SHOP BY ID
ShopModule.getShopById = (request) => {
  const shops = Parse.Object.extend('Shop');
  const query = new Parse.Query(shops)

  // find shop by id
  query.equalTo('objectId', request.params.id);

  // Get the Shop list
  return query.first()
    .then((resultShop) => {
      return resultShop
    })
    .catch((error) => {
      return error
    })
};

// REMOVE SHOP
ShopModule.removeShop = async (request) => {
  const shops = Parse.Object.extend('Shop');
  const query = new Parse.Query(shops)

  // Find shop by id
  query.equalTo('objectId', request.params.id);

  // Get the fist shop in query
  const shop = await query.first();
  if (shop) {

    // Change active = false 
    shop.set('active', false);

    // Save shop
    return shop.save()
      .then((shopRemoved) => {
        return shopRemoved
      })
      .catch((error) => {
        return error
      })
  } else {
    throw 'Shop not found';
  };
};

// UPDATE SHOP 
ShopModule.updateShop = async (request) => {
  const { id, name, address, district, ward, phone, openTime, closeTime, socialInfo, imageURL } = request.params;
  const shops = Parse.Object.extend('Shop');
  const query = new Parse.Query(shops)

  // Find shop by id
  query.equalTo('objectId', id);
  // Get the fist shop in query and update if found
  const shop = await query.first();


  if (shop) {
    shop.set('name', name);
    shop.set('address', address);
    shop.set('district', district);
    shop.set('ward', ward);
    shop.set('phone', phone);
    // shop.set('openTime', openTime);
    // shop.set('closeTime', closeTime);
    shop.set('socialInfo', socialInfo);
    shop.set('imageURL', imageURL);

    // Save shop
    return shop.save(null,{useMasterKey:true})
      .then((shopRemoved) => {
        return shopRemoved
      })
      .catch((error) => {
        return error
      })
  } else {
    throw 'Shop not found';
  };
};

ShopModule.SearchShop = request => {
  const {search} = request.params
  const shop = Parse.Object.extend('Shop')
  const query = new Parse.Query(shop)
  query.startsWith(search)
  return query.find().then((result) => {
    return result
  }).catch((err) => {
    return err
  });

}

ShopModule.getShopFromArray = request => {
  const {ids} = request.params
  const shop = Parse.Object.extend('Shop');
  const query = new Parse.Query(shop)
  query.containedIn('objectId', ids);
  return query.find()
}

ShopModule.getShopDetail = async request => {
 try {  
  const {shopId} = request.params
  const shop = Parse.Object.extend('Shop')
  const queryShop = new Parse.Query(shop)
  queryShop.equalTo('objectId',shopId)
  const shopDetail =  await queryShop.first()
  const product = Parse.Object.extend('Product')
  const queryProduct = new Parse.Query(product)
  queryProduct.equalTo('shop',shopDetail)
  queryProduct.equalTo('deleted',false)
  queryProduct.include('shop.lat')
  queryProduct.include('shop.address')
  queryProduct.include('shop.lnt')
  const productDetails = await queryProduct.find()
  return {
    shopDetail,
    productDetails
  }
} catch(err) {
  return err
}

}

module.exports = ShopModule;
