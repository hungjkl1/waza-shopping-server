const _ = require('lodash')

const RatingModule = {}

const createRatingShop = async request => {
    const {user} = request
    const {shopId, stars, comment} = request.params
    try {
        if(user&&shopId) {
         const shopClass = Parse.Object.extend('Shop')
         const queryShop = new Parse.Query(shopClass)
         queryShop.equalTo('objectId', shopId)
         const shop = await queryShop.first()
        if(shop) {
                const ratingShopClass = new Parse.Object.extend('ShopRating')    
                const ratingShop = new ratingShopClass()
                ratingShop.set('stars',stars)
                ratingShop.set('comment',comment)
                ratingShop.set('passenger',user)
                ratingShop.set('shop',shop)  
                const saveRating = await ratingShop.save(null,{useMasterket:true})
                return 'Done'
            }
        }
        return new Error('There are something wrong when rating this shop')
    } catch (err) {
        console.log(err)
        return err
    }
}