'use strict';

const parseServerOptions = require('./parse-server')

const serverURL = `${process.env.DEV_ENV}/api`
const parseDashboardOption = {
  mountPath: '/dashboard',
  "allowInsecureHTTP": true,
  apps: [
    {
      serverURL: parseServerOptions.serverURL || serverURL,
      appId: parseServerOptions.appId,
      masterKey: parseServerOptions.masterKey,
      javascriptKey: parseServerOptions.javascriptKey,
      restKey: parseServerOptions.restAPIKey,
      clientKey: parseServerOptions.clientKey,
      appName: 'wazashopping',
      appNameForURL: 'wazashopping'
    }
  ],
  users: [
    {
      user: 'wazaadmin',
      pass: '$2y$12$Tc0jxWRiooA66GdGFGkpnO2zNPu9EPkTgTLB5BK8e4aNyR3MxSXZG'
    }
  ],
  useEncryptedPasswords: true 
}
module.exports = parseDashboardOption;