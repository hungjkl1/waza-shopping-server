const serverURL=`${process.env.DEV_ENV}/api`
console.log('[i] - Server is on ' + serverURL)
const auth3rd = require('./auth3rd')
/**
|--------------------------------------------------
| PARSE CONFIG--
|--------------------------------------------------
*/
const parseServerOption = {
  databaseURI:'mongodb+srv://admin:rEp0xjWo9wdLBdfe@wazza-shopping-vrrga.mongodb.net/wazashopping-test?retryWrites=true&w=majority',
  cloud: './cloud/main.js',
  serverURL: process.env.SERVER_URL || 'http://localhost:3232/api' ,
  appId: process.env.APPID,
  masterKey: process.env.MASTER_KEY,
  javascriptKey: process.env.JS_KEY,
  restAPIKey: process.env.RESTAPI_KEY,
  clientKey: process.env.CLIENT_KEY,
  auth: {'core':{module:auth3rd} },
  liveQuery: {
    classNames: ['Bill','Shop','DriverList']
  }
}
/**
|--------------------------------------------------
| PARSE CONFIG--
|--------------------------------------------------
*/
module.exports = parseServerOption